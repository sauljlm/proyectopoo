package cr.ac.ucenfotec.coi.ui;

import cr.ac.ucenfotec.coi.bl.Atleta;

import java.io.*;
import java.util.ArrayList;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    static ArrayList<Atleta> atletas;

    public static void main(String[] args) throws IOException {
        atletas = new ArrayList<>();

        int opcion;

        do {
            opcion = pintarMenu();
            switch (opcion) {
                case 1:
                    agregarAtleta();
                    break;
                case 2:
                    mostrarAtletas();
                    break;
            }
        } while (opcion != 0);
    }

    public static int pintarMenu() throws IOException {
        out.println("--- MENU ---");
        out.println("1. Agregar atleta");
        out.println("2. Mostrar atletas");
        out.println("0. Salir");

        return Integer.parseInt(in.readLine());
    }

    public static void agregarAtleta() throws IOException {
        out.print("Nombre: ");
        String nombre = in.readLine();
        out.print("Edad: ");
        int edad = Integer.parseInt(in.readLine());
        out.print("Raza: ");
        String raza = in.readLine();
        out.print("Cantidad de huesos que come en un día: ");
        int cantHuesos = Integer.parseInt(in.readLine());

        Atleta atleta = new Atleta();

        atletas.add(atleta);
    }

    public static void mostrarAtletas() {
        out.println("");
        out.println("* Atletas *");
        for(Atleta perro: atletas) {
            out.print(perro.getNombre() + ", ");
        }
        out.println("");
        out.println("");
    }
}
