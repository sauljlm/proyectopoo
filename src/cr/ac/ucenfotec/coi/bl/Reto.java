package cr.ac.ucenfotec.coi.bl;

import java.util.ArrayList;
import java.util.Objects;

public class Reto extends Catalogo{
    private String nombre;
    private String descripcion;
    private double kilometros;
    private String medalla;
    private int precio;
    private String puntoInicio;
    private String puntoFin;
    private ArrayList<Hito> hitos;

    public Reto() {
        super();
        setNombre("sin nombre");
        setDescripcion("sin descripcion");
        setKilometros(0);
        setMedalla("sin medalla");
        setPrecio(0);
        setPuntoInicio("sin puntoInicio");
        setPuntoFin("sin puntoFin");
        setHitos(null);
    }

    public Reto(String nombre, String descripcion, String medalla, int precio, String puntoInicio, String puntoFin, String codigo) {
        super(codigo);
        setNombre(nombre);
        setDescripcion(descripcion);
        setMedalla(medalla);
        setPrecio(precio);
        setPuntoInicio(puntoInicio);
        setPuntoFin(puntoFin);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getKilometros() {
        return kilometros;
    }

    public void setKilometros(double kilometros) {
        this.kilometros = kilometros;
    }

    public String getMedalla() {
        return medalla;
    }

    public void setMedalla(String medalla) {
        this.medalla = medalla;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getPuntoInicio() {
        return puntoInicio;
    }

    public void setPuntoInicio(String puntoInicio) {
        this.puntoInicio = puntoInicio;
    }

    public String getPuntoFin() {
        return puntoFin;
    }

    public void setPuntoFin(String puntoFin) {
        this.puntoFin = puntoFin;
    }

    public ArrayList<Hito> getHitos() {
        return hitos;
    }

    public void setHitos(ArrayList<Hito> hitos) {
        this.hitos = hitos;
    }

    public void  calcularKilometros() {
        // TODO: calcular kilometros
    }

    public void  agregarHido() {
        // TODO: agregar hito
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reto reto = (Reto) o;
        return Double.compare(reto.getKilometros(), getKilometros()) == 0 && Objects.equals(getNombre(), reto.getNombre());
    }

    @Override
    public String toString() {
        return "Reto{" +
                "nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", kilometros=" + kilometros +
                ", medalla='" + medalla + '\'' +
                ", precio=" + precio +
                ", puntoInicio='" + puntoInicio + '\'' +
                ", puntoFin='" + puntoFin + '\'' +
                ", hitos=" + hitos +
                '}';
    }
}
