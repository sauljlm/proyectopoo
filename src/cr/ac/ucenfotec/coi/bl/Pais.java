package cr.ac.ucenfotec.coi.bl;

import java.util.ArrayList;
import java.util.Objects;

public class Pais extends Catalogo{
    private String nombre;
    private ArrayList<Provincia> provincias;

    public Pais() {
        super();
        setNombre("sin nombre");
        setProvincias(null);
    }

    public Pais(String nombre, String codigo) {
        super(codigo);
        setNombre(nombre);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Provincia> getProvincias() {
        return provincias;
    }

    public void setProvincias(ArrayList<Provincia> provincias) {
        this.provincias = provincias;
    }

    public void agregarProvincias() {
        // TODO: agregar provincias
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Pais pais = (Pais) o;
        return Objects.equals(getNombre(), pais.getNombre());
    }

    @Override
    public String toString() {
        return "Pais{" +
                "nombre='" + nombre + '\'' +
                ", provincias=" + provincias +
                '}';
    }
}
