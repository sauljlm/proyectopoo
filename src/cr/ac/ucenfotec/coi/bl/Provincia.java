package cr.ac.ucenfotec.coi.bl;

import java.util.ArrayList;
import java.util.Objects;

public class Provincia extends Catalogo{

    private String nombre;
    private ArrayList<Canton> cantones;

    public Provincia() {
        super();
        setNombre("sin nombre");
        setCantones(null);
    }

    public Provincia(String nombre, String codigo) {
        super(codigo);
        setNombre(nombre);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Canton> getCantones() {
        return cantones;
    }

    public void setCantones(ArrayList<Canton> cantones) {
        this.cantones = cantones;
    }

    public void agregarCantones() {
        // TODO: agregar cantones
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Provincia provincia = (Provincia) o;
        return Objects.equals(getNombre(), provincia.getNombre());
    }

    @Override
    public String toString() {
        return "Provincia{" +
                "nombre='" + nombre + '\'' +
                ", cantones=" + cantones +
                '}';
    }
}
