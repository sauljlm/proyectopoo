package cr.ac.ucenfotec.coi.bl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Objects;

public class Atleta extends Usuario {
    private String avatar;
    private Date fechaNacimiento;
    private int edad;
    private String genero;
    private Direccion direccion;
    private Reto reto;
    private ArrayList<Tarjeta> metodosPago;

    public Atleta() {
        super();
        setAvatar("sin avatar");
        setFechaNacimiento(Date.valueOf("30/06/2021"));
        setEdad(Date.valueOf("30/06/2021"));
        setGenero("sin genero");
        setDireccion(null);
        setReto(null);
        setMetodosPago(null);
    }

    public Atleta(String avatar, Date fechaNacimiento, String genero, Direccion direccion, Reto reto, String nombre, String apellidos, String identificacion, String pais, String nombreUsuario, String contrasenia) {
        super(nombre, apellidos, identificacion, pais, nombreUsuario, contrasenia);
        setAvatar(avatar);
        setFechaNacimiento(fechaNacimiento);
        setEdad(fechaNacimiento);
        setGenero(genero);
        setDireccion(direccion);
        setReto(reto);
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(Date edad) {
        this.edad = 0;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public Reto getReto() {
        return reto;
    }

    public void setReto(Reto reto) {
        this.reto = reto;
    }

    public ArrayList<Tarjeta> getMetodosPago() {
        return metodosPago;
    }

    public void setMetodosPago(ArrayList<Tarjeta> metodosPago) {
        this.metodosPago = metodosPago;
    }

    public void agregarTarjeta() {
        // Todo: agregar tarjeta
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Atleta atleta = (Atleta) o;
        return getEdad() == atleta.getEdad() && Objects.equals(getAvatar(), atleta.getAvatar()) && Objects.equals(getFechaNacimiento(), atleta.getFechaNacimiento()) && Objects.equals(getGenero(), atleta.getGenero()) && Objects.equals(getReto(), atleta.getReto()) && Objects.equals(getMetodosPago(), atleta.getMetodosPago());
    }

    @Override
    public String toString() {
        return "Atleta{" +
                "avatar='" + avatar + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", genero='" + genero + '\'' +
                ", reto=" + reto +
                ", metodosPago=" + metodosPago +
                '}';
    }
}
