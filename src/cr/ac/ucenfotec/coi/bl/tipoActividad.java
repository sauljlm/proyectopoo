package cr.ac.ucenfotec.coi.bl;

import java.util.Objects;

public class tipoActividad {
    private String icono;
    private String nombre;

    public tipoActividad() {
        setIcono("sin icono");
        setNombre("sin nombre");
    }

    public tipoActividad(String icono, String nombre) {
        setIcono(icono);
        setNombre(nombre);
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        tipoActividad that = (tipoActividad) o;
        return Objects.equals(getNombre(), that.getNombre());
    }

    @Override
    public String toString() {
        return "tipoActividad{" +
                "icono='" + icono + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
