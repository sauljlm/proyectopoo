package cr.ac.ucenfotec.coi.bl;

import java.sql.Date;
import java.util.Objects;

public class Tarjeta {
    private String numeroTarjeta;
    private String proveedor;
    private Date fechaVencimiento;
    private String codigoCVV;

    public Tarjeta() {
        setNumeroTarjeta("sin numeroTarjeta");
        setProveedor("sin proveedor");
        setFechaVencimiento(Date.valueOf("01/01/1"));
        setCodigoCVV("sin codigoCVV");
    }

    public Tarjeta(String numeroTarjeta, String proveedor, Date fechaVencimiento, String codigoCVV) {
        setNumeroTarjeta(numeroTarjeta);
        setProveedor(proveedor);
        setFechaVencimiento(fechaVencimiento);
        setCodigoCVV(codigoCVV);
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getCodigoCVV() {
        return codigoCVV;
    }

    public void setCodigoCVV(String codigoCVV) {
        this.codigoCVV = codigoCVV;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tarjeta tarjeta = (Tarjeta) o;
        return Objects.equals(getNumeroTarjeta(), tarjeta.getNumeroTarjeta());
    }

    @Override
    public String toString() {
        return "Tarjeta{" +
                "numeroTarjeta='" + numeroTarjeta + '\'' +
                ", proveedor='" + proveedor + '\'' +
                ", fechaVencimiento=" + fechaVencimiento +
                ", codigoCVV='" + codigoCVV + '\'' +
                '}';
    }
}
