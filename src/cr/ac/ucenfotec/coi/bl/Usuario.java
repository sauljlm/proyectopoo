package cr.ac.ucenfotec.coi.bl;

import java.util.Objects;

public class Usuario {
    private String nombre;
    private String apellidos;
    private String identificacion;
    private String pais;
    private String nombreUsuario;
    private String contrasenia;

    public Usuario() {
        setNombre("Sin nombre");
        setApellidos("Sin apellidos");
        setIdentificacion("Sin identificacion");
        setPais("Sin pais");
        setNombreUsuario("Sin nombreUsiario");
        setContrasenia("Sin contrasenia");
    }

    public Usuario(String nombre, String apellidos, String identificacion, String pais, String nombreUsuario, String contrasenia) {
        setNombre(nombre);
        setApellidos(apellidos);
        setIdentificacion(identificacion);
        setPais(pais);
        setNombreUsuario(nombreUsuario);
        setContrasenia(contrasenia);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return Objects.equals(getIdentificacion(), usuario.getIdentificacion());
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", pais='" + pais + '\'' +
                ", nombreUsuario='" + nombreUsuario + '\'' +
                ", contrasenia='" + contrasenia + '\'' +
                '}';
    }
}
