package cr.ac.ucenfotec.coi.bl;

import java.util.ArrayList;
import java.util.Objects;

public class Canton extends Catalogo{

    private String nombre;
    private ArrayList<Distrito> distritos;

    public Canton() {
        super();
        setNombre("sin nombre");
        setDistritos(null);
    }

    public Canton(String nombre, String codigo) {
        super(codigo);
        setNombre(nombre);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Distrito> getDistritos() {
        return distritos;
    }

    public void setDistritos(ArrayList<Distrito> distritos) {
        this.distritos = distritos;
    }

    public void agregarDistritos() {
        // TODO: agregar distritos
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Canton canton = (Canton) o;
        return Objects.equals(getNombre(), canton.getNombre());
    }

    @Override
    public String toString() {
        return "Canton{" +
                "nombre='" + nombre + '\'' +
                ", distritos=" + distritos +
                '}';
    }
}
