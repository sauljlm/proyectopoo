package cr.ac.ucenfotec.coi.bl;

import java.util.Objects;

public class Direccion {
    private String detalle;
    private String pais;
    private String provincia;
    private String canton;
    private String distrito;

    public Direccion() {
        setDetalle("sin detalle");
        setPais("sin pais");
        setProvincia("sin provincia");
        setCanton("sin canton");
        setDistrito("sin distrito");
    }

    public Direccion(String detalle, String pais, String provincia, String canton, String distrito) {
        setDetalle(detalle);
        setPais(pais);
        setProvincia(provincia);
        setCanton(canton);
        setDistrito(distrito);
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Direccion direccion = (Direccion) o;
        return Objects.equals(getDetalle(), direccion.getDetalle()) && Objects.equals(getPais(), direccion.getPais()) && Objects.equals(getProvincia(), direccion.getProvincia()) && Objects.equals(getCanton(), direccion.getCanton()) && Objects.equals(getDistrito(), direccion.getDistrito());
    }

    @Override
    public String toString() {
        return "Direccion{" +
                "detalle='" + detalle + '\'' +
                ", pais='" + pais + '\'' +
                ", provincia='" + provincia + '\'' +
                ", canton='" + canton + '\'' +
                ", distrito='" + distrito + '\'' +
                '}';
    }
}
