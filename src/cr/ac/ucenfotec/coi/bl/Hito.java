package cr.ac.ucenfotec.coi.bl;

import java.util.Objects;

public class Hito {
    private String nombre;
    private int kilometro;
    private String imagen;
    private String descripcion;
    private String paginaWeb;

    public Hito() {
        setNombre("sin nombre");
        setKilometro(0);
        setImagen("sin imagen");
        setDescripcion("sin descripcion");
        setPaginaWeb("sin paginaWeb");
    }

    public Hito(String nombre, int kilometro, String imagen, String descripcion, String paginaWeb) {
        setNombre(nombre);
        setKilometro(kilometro);
        setImagen(imagen);
        setDescripcion(descripcion);
        setPaginaWeb(paginaWeb);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getKilometro() {
        return kilometro;
    }

    public void setKilometro(int kilometro) {
        this.kilometro = kilometro;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPaginaWeb() {
        return paginaWeb;
    }

    public void setPaginaWeb(String paginaWeb) {
        this.paginaWeb = paginaWeb;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hito hito = (Hito) o;
        return getKilometro() == hito.getKilometro() && Objects.equals(getNombre(), hito.getNombre());
    }

    @Override
    public String toString() {
        return "Hito{" +
                "nombre='" + nombre + '\'' +
                ", kilometro=" + kilometro +
                ", imagen='" + imagen + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", paginaWeb='" + paginaWeb + '\'' +
                '}';
    }
}
