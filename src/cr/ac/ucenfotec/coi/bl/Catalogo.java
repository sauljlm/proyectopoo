package cr.ac.ucenfotec.coi.bl;

import java.util.Objects;

public class Catalogo {

    private String codigo;

    public Catalogo() {
        setCodigo("sin codigo");
    }

    public Catalogo(String codigo) {
        setCodigo(codigo);
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Catalogo catalogo = (Catalogo) o;
        return Objects.equals(getCodigo(), catalogo.getCodigo());
    }

    @Override
    public String toString() {
        return "Catalogo{" +
                "codigo='" + codigo + '\'' +
                '}';
    }
}
