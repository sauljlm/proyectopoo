package cr.ac.ucenfotec.coi.bl;

import java.util.Objects;

public class Distrito extends Catalogo {
    private String nombre;

    public Distrito() {
        super();
        setNombre("sin nombre");
    }

    public Distrito(String nombre, String codigo) {
        super(codigo);
        setNombre(nombre);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Distrito distrito = (Distrito) o;
        return Objects.equals(getNombre(), distrito.getNombre());
    }

    @Override
    public String toString() {
        return "Distrito{" +
                "nombre='" + nombre + '\'' +
                '}';
    }
}
